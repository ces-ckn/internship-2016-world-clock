var express = require('express');
var wagner = require('wagner-core');
var session = require('express-session');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var mongoose = require('mongoose');
var status = require('http-status');
require('./model/models')(wagner);
var app = express();
// Set up a URL route

app.use(cookieParser('ssshhhhh'));

//end cookie region

app.use(express.static('public'));

app.use(express.static(__dirname + '/public'));

app.use('/googlemap', express.static(__dirname + '/public/googlemap.html'));
app.use('/indexdraft', express.static(__dirname + '/public/indexdraft.html'));

app.use('/main', express.static(__dirname + '/public/main.html'));

app.use('/login', express.static(__dirname + '/public/login.html'));

app.use('/event', express.static(__dirname + '/public/event master.html'));

/*
Linh session

*/


/* -------------------- */
app.use(session({secret: 'ssshhhhh'}));
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
})); 


app.use('/api/v1', require('./api')(wagner));
app.use(express.static('./', { maxAge: 4 * 60 * 60 * 1000 /* 2hrs */ }));

// For testing
app.use('/test', express.static(__dirname + '/public/testclosure.html'));
// bind the app to listen for connections on a specified port

var sess;
var sessLogin;

app.get("/getLocationsSession", function(req, res) {
    sess = req.session;
    if(sess.locations){        
        res.send(sess.locations);     
    }else{
        sess.locations = [];
        res.send(sess.locations); 
    }
    /*if(req.cookies.locations === undefined){
        res.cookie('locations', new Array(), {maxAge: 900000});
    }
    console.log('Cookies: ', req.cookies);
    res.send(req.cookies.locations);*/
});

app.post("/saveLocationsToSession", function(req, res) {
    sess = req.session;
    sess.locations = req.body.locations;
    res.end('done');
    /*res.cookie('locations', req.body.locations, {maxAge: 900000});
    console.log('Cookies: ', req.cookies.locations);
    res.send('done');*/
});

var User = mongoose.model('User', require('./model/user'), 'users');
var Event = mongoose.model('Event', require('./model/event'), 'events');
app.post("/saveLocationsToUser", function(req, res) {
    sessLogin = req.session;
    User.findById(sessLogin._id, function(error, user) {
        if (error) {
            console.log('error here: '+ error)
        }
        if (!user) {
            console.log('!user')
        }
        user.locations = req.body.locations;
        user.save(function(err) {
            if (err)
                res.send(err);
        });
    });
    res.end('done');
});

app.get('/getLoggedInUser', function(req, res){
    sessLogin = req.session;
    if(sessLogin.username){
        User.findById(sessLogin._id, function(error, user) {
            if (error) {
                return res.
                status(status.INTERNAL_SERVER_ERROR).
                json({ error: error.toString() });
            }
            if (!user) {
                return res.
                    status(status.NOT_FOUND).
                    json({ error: 'Not found' });
            }
            res.json({ user: user });        
        });
    }else{
        console.log('The is no user logging in');
        res.json({user: 'false'});
    }
});

app.post('/login', function(req,res){
    var x = 'global';
    var username;
    console.log(req.body.user);
    sessLogin = req.session;
    sessLogin.username = 'something';

    User.findOne(req.body.user, function(error, user) {
        if (error) {
            return res.
            status(status.INTERNAL_SERVER_ERROR).
            json({ error: error.toString() });
        }
        if (!user) {
            return res.
                status(status.NOT_FOUND).
                json({ error: 'Not found' });
        }
        console.log('session login before: ' + sessLogin.username);        
        sessLogin.username = req.body.user.username;
        sessLogin._id = user._id;
        console.log('session login after: ' + sessLogin.username);
        res.json({ user: user });        
    });
  });

app.get('/logout',function(req,res){
    req.session.destroy(function(err) {
      if(err) {
        console.log(err);
      } else {
        res.redirect('/');
      }
    });
});            

app.get('/admin',function(req,res){
    sessLogin = req.session;
    console.log(sessLogin.username);
    if(sessLogin.username == 'cubeit') {
        res.write('<h1>Hello ' + sessLogin.username + '</h1>');
        res.end('<a href="+">Logout</a>');
    } else {
        res.write('<h1>Please login first.</h1>');
        res.end('<a href="+">Login</a>');
    }
});

app.post('/saveDateToSession',function(req,res){
    sess = req.session;
    sess.date = req.body.date;
    res.end('done');
});

app.get('/getDateFromSession',function(req,res){
    if(req.session.date){
        res.send(req.session.date);
    }else{
        res.send(new Date());
    }
});

app.post('/saveEvent', function(req, res){
    var event = new Event({
            title: "Let's meet!",
            locations: req.body.locations,
            description: "Please be on time",
            homeTimeRange: req.body.homeTimeRange,
            homeDaysRange: req.body.homeDaysRange
        });
        //console.log(event);
        event.save(function(error, event) {
            if (error) {
              return res.
                status(status.INTERNAL_SERVER_ERROR).
                json({ error: error.toString()});
            }
            res.send({_id: event._id});
        });
});

app.get('/getEventById/:id', function(req, res){
    //console.log(req.params.id)
    Event.findById(req.params.id, function(error, event) {
        if (error) {
            console.log('error here: '+ error)
        }
        if (!event) {
            console.log('!event')
        }        
        res.send({event: event});
    });    
});

app.post('/saveEventInfo/:id', function(req, res){
    Event.findById(req.params.id, function(error, event) {
        if (error) {
            console.log('error here: '+ error)
        }
        if (!event) {
            console.log('!event')
        }      

        event.title = req.body.title;
        event.description = req.body.description;

        event.save(function(error, saved_event) {
            if (error) {
              return res.
                status(status.INTERNAL_SERVER_ERROR).
                json({ error: error.toString()});
            }
            res.send({_id: saved_event._id});
        });

    });   
        
});

var port = process.env.PORT || 3000;
app.listen(port);   

// Render some console log output
console.log("Listening on port " + port);