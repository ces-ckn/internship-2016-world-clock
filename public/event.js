var app = angular.module('myApp', ['ng', 'ngRoute']);

app.config( function ( $routeProvider ) {
  $routeProvider
  .when( '/:id', {
      controller: 'EventController',
      templateUrl: 'event.html' 
  })
  .when( '/editEvent/:id', {
      controller: 'EventController',
      templateUrl: 'edit event.html' 
  });
});

app.controller('EventController', ['$scope', '$route', '$routeParams', '$http', '$location', function($scope, $route, $routeParams, $http, $location){
	var bitlyToken = 'd775ae0625f62de12b689ae22320f3e6bb15a536';
	$scope.shortLink = false;
	$scope.id;
	$scope.title;
	$scope.description;
	$scope.locations;
	$scope.home;
	$scope.homeTimeRange;
	$scope.homeDaysRange;
    //just to make sure that we have got id before doing something else =='
    //setTimeout(function(){
    	$scope.id = $routeParams.id;
    	$http({
		    url: '/getEventById/' + $scope.id, 
	    	method: "GET"
 		}).then(function(res){
 			var event = res.data.event;
 			$scope.title = event.title;
 			$scope.description = event.description;
 			$scope.locations = event.locations;
 			$scope.homeTimeRange = event.homeTimeRange;
			$scope.homeDaysRange = event.homeDaysRange;
 			angular.forEach($scope.locations, function(location, index){
 				if(location.isHome){
 					$scope.home = location;
 				}
 			});
 		});
 //   }, 10);

    addZero = function(obj){
        return ((obj < 10) ? ("0"+obj) : obj);
    }

    $scope.calTimeRange = function(calculatingTimeZoneId){
    	var timeRange         = $scope.homeTimeRange;
        var dayRange          = $scope.homeDaysRange;
        var home 			  = $scope.home;
        var timeToReturn      = "";
        var datesMoment       = "";
        var beginDate         = dayRange.split('-')[0];
        var endDate           = dayRange.split('-')[1];
        var beginTime         = timeRange.split('-')[0];
        var endTime           = timeRange.split('-')[1];
        var date              = new Date(beginDate);
        //datesGoogle = datesGoogle + date.getFullYear()+(addZero(date.getMonth()+1))+addZero(date.getDate());
        datesMoment = date.getFullYear()+"-"+(addZero(date.getMonth()+1))+"-"+addZero(date.getDate());
        var m = moment.tz(datesMoment + " "+ beginTime, home.timeZoneId);
        m.tz(calculatingTimeZoneId);
        timeToReturn = timeToReturn + m.format("HH:mm");
        date = new Date(endDate);
        datesMoment = date.getFullYear()+"-"+(addZero(date.getMonth()+1))+"-"+addZero(date.getDate());
        m = moment.tz(datesMoment + " "+ endTime, home.timeZoneId);
        m.tz(calculatingTimeZoneId);
        timeToReturn = timeToReturn + " - " + m.format("HH:mm");
        return timeToReturn;
    }

    $scope.calDaysRange = function(calculatingTimeZoneId){
    	var timeRange         = $scope.homeTimeRange;
        var dayRange          = $scope.homeDaysRange;
        var home 			  = $scope.home;
        var dayToReturn       = "";
        var datesMoment       = "";
        var beginDate         = dayRange.split('-')[0];
        var endDate           = dayRange.split('-')[1];
        var beginTime         = timeRange.split('-')[0];
        var endTime           = timeRange.split('-')[1];
        var date              = new Date(beginDate);
        //datesGoogle = datesGoogle + date.getFullYear()+(addZero(date.getMonth()+1))+addZero(date.getDate());
        datesMoment = date.getFullYear()+"-"+(addZero(date.getMonth()+1))+"-"+addZero(date.getDate());
        var m = moment.tz(datesMoment + " "+ beginTime, home.timeZoneId);
        m.tz(calculatingTimeZoneId);
        dayToReturn = dayToReturn + m.format("ddd, MMM DD YYYY");
        //timeToReturn = timeToReturn + m.format("ddd, MMM YYYY");
        date = new Date(endDate);
        datesMoment = date.getFullYear()+"-"+(addZero(date.getMonth()+1))+"-"+addZero(date.getDate());
        m = moment.tz(datesMoment + " "+ endTime, home.timeZoneId);
        m.tz(calculatingTimeZoneId);
        dayToReturn = dayToReturn + " - " + m.format("ddd, MMM DD YYYY");
        return dayToReturn;
    }

    $scope.saveEventInfo = function(){
    	$http({
            method: 'POST',
            url: '/saveEventInfo/'+$scope.id,
            data: {
                title: $scope.title,
                description: $scope.description
            }
        }).then(function(res){
            window.location = '/event#/'+res.data._id;
        });
    }

    $scope.getShortLink = function(){
    	longURL = $location.absUrl();
    	API_Address = 'https://api-ssl.bitly.com';
    	GET_Address = '/v3/shorten?access_token='+ bitlyToken +'&longUrl=';
    	option = '&format=txt';
    	$http({
            method: 'GET',
            url: API_Address + GET_Address + window.encodeURIComponent(longURL) + option,
        }).then(function(res){
            $scope.shortLink = res.data;
        });
    }

    $scope.addYourTime = function(){
        var user_location = {
            isHome: false,
            country: "Your device/machine",
            location: "Your Time",
            timeZoneId: moment.tz.guess()
        }
        $scope.locations.push(user_location);
    }

}]);
