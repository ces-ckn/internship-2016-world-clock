var app = angular.module('myApp', ['ng', 'ngRoute']);

app.config(function($routeProvider){
    $routeProvider.
    when('/', {
        
    })
    .when('/register', {
        templateUrl: '/public/account/register/register.html',
        controller: 'AccountController'
    });
});

app.controller('AccountController', function($scope, $http) {
    $scope.user = {};

    $scope.$back = function() { 
        window.history.back();
    };

    $scope.register = function(){
    $http.post('/api/v1/register', $scope.user)
         .success(function (data, status, headers, config) {
            window.location = '/public/index.html';
         })
         .error(function (data) {
            $scope.errorMsg = data.error;
         });
    }
});