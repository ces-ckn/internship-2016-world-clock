var app = angular.module('myApp', ['ng', 'ngRoute']);

app.config(function($routeProvider){
    $routeProvider.
    when('/', {
        templateUrl: '/public/home.html'
    })
    .when('/register', {
        templateUrl: '/public/account/register/register.html',
        controller: 'AccountController'
    })
});

app.controller('AccountController', function($scope, $http) {
    $scope.user = {};
    $scope.$back = function() { 
        window.history.back();
    };

    $scope.testPost = function(){
    $http.post('/api/v1/testPost', $scope.user)
         .success(function (data, status, headers, config) {
            
         })
         .error(function (data) {
            $scope.errorMsg = data.error;
         });
    }
});
