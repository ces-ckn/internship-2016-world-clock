var app = angular.module('myApp', ['ng', 'ngRoute']);

app.controller('MainController', function($scope, $http) {
    $scope.locations = [];  
    $scope.user;
    $scope.date = new Date();

    $scope.findHome = function (){
        var homeIndex = 0;
        angular.forEach($scope.locations,function(location, index){
            if(location.isHome == true){
                homeIndex = index;                        
            }    
        });
        return homeIndex;       
    }    

   $http.get('/getDateFromSession').then(function(res){
        $scope.date = new Date(res.data);
    });  
    
    var standardZone = 'Asia/Ho_Chi_Minh';
    var tiles = [];
    var tilesView = [];

    $scope.currentSelectedDate;
    $scope.timeStamp = $scope.date / 1000;

    //get user if logged in
    $http.get('/getLoggedInUser').then(function(res){
        if(res.data.user == 'false'){
            $scope.user = false;
            //if reload the page, load locations from session    
            $http.get('/getLocationsSession').then(function(res){
                $scope.locations = res.data;
                //update the current time
                angular.forEach($scope.locations,function(location, index){
                    location.currentTime = moment.tz(new Date(), location.timeZoneId).format("HH:mm");            
                });

                angular.forEach($scope.locations, function(location, index){
                    location.timeRangeView = ''; 
                });

                $scope.timeStamp = $scope.date / 1000; 
            });    
        }else{
            console.log(res.data.user);
            $scope.user = res.data.user;     
            $scope.locations = res.data.user.locations;
            angular.forEach($scope.locations,function(location, index){
                location.currentTime = moment.tz(new Date(), location.timeZoneId).format("HH:mm");            
            });

            angular.forEach($scope.locations, function(location, index){
                location.timeRangeView = ''; 
            });

            $scope.timeStamp = $scope.date / 1000;             
        }
    });    
    
    $scope.addLocation = function (location) {
        $scope.$apply(function () {
            var timestamp = $scope.date;
            var momentFormatted = moment.tz(timestamp, location.timezone.timeZoneId);

            var yesterday = new Date($scope.date.getTime());
            yesterday.setDate(yesterday.getDate() - 1);

            var today = new Date($scope.date.getTime());

            var tomorrow = new Date($scope.date.getTime());
            tomorrow.setDate(tomorrow.getDate() + 1); 

            // Yesterday
            var timeStringTemp = $scope.getTimeString(yesterday);        
            $scope.pushTiles(tiles, standardZone, location.timezone.timeZoneId, timeStringTemp);

            // Today
            timeStringTemp = $scope.getTimeString(today);
            $scope.pushTiles(tiles, standardZone, location.timezone.timeZoneId, timeStringTemp);

            // Tomorrow
            timeStringTemp = $scope.getTimeString(tomorrow);
            $scope.pushTiles(tiles, standardZone, location.timezone.timeZoneId, timeStringTemp);                                     

            $scope.locations.push({
                location: location.location,
                country: location.country,
                currentDate: momentFormatted.format("ddd, MMM DD"),
                currentTime: momentFormatted.format("HH:mm"),
                isHome: location.isHome,
                timezone_z: momentFormatted.format("z"), //timezone_z is: ICT, IST, ACWST, NZST, etc.
                timezone_Z: momentFormatted.format("Z"), //timezone_Z is: +03:00, +05:30, -08:15, etc.
                timeZoneId: location.timezone.timeZoneId,
                tiles: tiles,
                tilesView: tilesView,
                timeRange: '',
                timeRangeView: ''
            });

            tiles = [];

            var homeIndex = $scope.findHome();
            $scope.updateTilesView(homeIndex);              
            
            if($scope.user != false){
                $scope.saveLocationsToUser();
            }else{
                $scope.saveLocationsToSession();
            }            
        });
    } // end of addLocation

    //this function is used to update the time displayed 
    var currentColor= 'black';
    var temp;
    setInterval(function(){

        var d = new Date();
        if(d.getSeconds() == 0){
            angular.forEach($scope.locations,function(location, index){
                location.currentTime = moment.tz(new Date(), location.timeZoneId).format("HH:mm");
                //re-render angular view
                $scope.$digest();
            }); 
        }

        $('.blinker').each(function(){
            $(this).removeClass();
            $(this).addClass('blinker');
            $(this).addClass(currentColor);
        });
        currentColor = (currentColor == 'black' ? 'gray' : 'black'); 
    }, 1000); 

    $scope.removeLocation = function(index){
        $scope.locations.splice(index, 1);
        if($scope.user == false){
            $scope.saveLocationsToSession();
        }else{
            $scope.saveLocationsToUser();
        }
    }

    $scope.setHome = function(newHomeIndex){
        var homeIndex = newHomeIndex;
        angular.forEach($scope.locations,function(location, index){
            if(index == newHomeIndex){
                location.isHome = true;
            }else{
                location.isHome = false;
            }    
        });

        $scope.updateTilesView(homeIndex);

        if($scope.user == false){
            $scope.saveLocationsToSession();
        }else{
            $scope.saveLocationsToUser();
        }
    }

    //save location to session so that it cant be lost after reload the page
    $scope.saveLocationsToSession = function(){
        //console.log($scope.locations)
        $http({
            method: 'POST',
            url: '/saveLocationsToSession',
            data: {
                locations: $scope.locations
            }
        });
    }

    $scope.saveLocationsToUser = function(){
        $http({
            method: 'POST',
            url: '/saveLocationsToUser',
            data: {
                locations: $scope.locations
            }
        });        
    }

    /*
        function calTimezone
        input: index of the location is being calculated
        output: the different timezone offset between this location and the home location
    */
    $scope.calTimezone = function(calculatingIndex){  
        var home;
        var hasHome = false;
        var result;

        //determine home location by loop through scope
        angular.forEach($scope.locations,function(location, index){
            if(location.isHome){
                home = location;
                hasHome = true;
            }   
        });

        angular.forEach($scope.locations,function(location, index){
            if((index == calculatingIndex) && hasHome){         
                result = calTimezone(location.timezone_Z, home.timezone_Z);
                return;
            }   
        });
        return result;
    }

    /*
        function calTimezone
        input: timezone_Z of the location is being calculated, timezone_Z of the home location
        output: the different timezone offset between this location and the home location
    */

    calTimezone = function(location_timezone_Z, home_timezone_Z){
        var t1 = location_timezone_Z.split(':');
        var t2 = home_timezone_Z.split(':');
        var int_t1 = parseInt(t1[0]);
        var int_t2 = parseInt(t2[0]);
        switch(t1[1]){
            case "15": int_t1 += 0.25; break;
            case "30": int_t1 += 0.5; break;
            case "45": int_t1 += 0.75; break;
        }
        switch(t2[1]){
            case "15": int_t2 += 0.25; break;
            case "30": int_t2 += 0.5; break;
            case "45": int_t2 += 0.75; break;
        }
        var result = int_t1 - int_t2;
        return (result >= 0 ? ("+" + result) : result);
    }

    //sort ASC by timezone
    $scope.sortAsc = function(){
        var home;
        var hasHome = false;
        var result;
        angular.forEach($scope.locations,function(location, index){
            if(location.isHome){
                home = location;
                hasHome = true;
            }   
        }); 
        if(!hasHome){
            alert('Please select the home UTC');
            return;
        }
        /*
            sort by timezone using sort function
            for more information, see this article: https://davidwalsh.name/array-sort
            documentation of sort(function()): http://www.w3schools.com/jsref/jsref_sort.asp
        */
        return $scope.locations.sort(function(obj1, obj2){
            return calTimezone(obj1.timezone_Z, home.timezone_Z) - calTimezone(obj2.timezone_Z, home.timezone_Z);
        });
    }

    $scope.sortDesc = function(){
        //simply reverse the array
        return $scope.sortAsc().reverse();
    }

    $scope.hasLocationInList = function(){
        return $scope.locations.length > 0;
    }

    addZero = function(obj){
        return ((obj < 10) ? ("0"+obj) : obj);
    }

    getGoogleDateBaseOnHome = function(home){
        var userTimezoneGuess = moment.tz.guess();
        var timeRange         = $scope.homeTimeRange;
        var dayRange          = $scope.homeDaysRange;
        var datesGoogle       = "";
        var datesMoment       = "";
        var beginDate         = dayRange.split('-')[0];
        var endDate           = dayRange.split('-')[1];
        var beginTime         = timeRange.split('-')[0];
        var endTime           = timeRange.split('-')[1];
        var date              = new Date(beginDate);
        //datesGoogle = datesGoogle + date.getFullYear()+(addZero(date.getMonth()+1))+addZero(date.getDate());
        datesMoment = date.getFullYear()+"-"+(addZero(date.getMonth()+1))+"-"+addZero(date.getDate());
        var m = moment.tz(datesMoment + " "+ beginTime, home.timeZoneId);
        m.tz(userTimezoneGuess);
        datesGoogle = datesGoogle + m.format("YYYYMMDD")+"T"+m.format("HHmm")+"00"; //00 is default second
        date = new Date(endDate);
        datesMoment = date.getFullYear()+"-"+(addZero(date.getMonth()+1))+"-"+addZero(date.getDate());
        m = moment.tz(datesMoment + " "+ endTime, home.timeZoneId);
        m.tz(userTimezoneGuess);
        datesGoogle = datesGoogle + "/"+ m.format("YYYYMMDD")+"T"+m.format("HHmm")+"00"; 
        return datesGoogle;
    }

    renderGoogleCalendarURL = function(title, dates, home, locations){
        var timeRange           = $scope.homeTimeRange;
        var dayRange            = $scope.homeDaysRange;
        var beginTime           = timeRange.split('-')[0];
        var endTime             = timeRange.split('-')[1];
        var begin_date          = new Date(dayRange.split('-')[0]);
        var end_date            = new Date(dayRange.split('-')[1]);
        var begin_datesMoment   = begin_date.getFullYear()+"-"+(addZero(begin_date.getMonth()+1))+"-"+addZero(begin_date.getDate());
        var end_datesMoment     = end_date.getFullYear()+"-"+(addZero(end_date.getMonth()+1))+"-"+addZero(end_date.getDate());
                    
        var returnHTML = "https://calendar.google.com/calendar/render?action=TEMPLATE";
        returnHTML = returnHTML + "&text=" + title;
        returnHTML = returnHTML + "&dates=" + dates;
        returnHTML = returnHTML + "&details=";
        
        var details = "";

        angular.forEach(locations,function(location, index){
            details = details + location.location + ", " + location.country + "%0A"; //%0A = enter = new line
            var m = moment.tz(begin_datesMoment + " "+ beginTime, home.timeZoneId);
            m.tz(location.timeZoneId);
            details = details + m.format("HH:mm") + "%09"; //%09 = \t = tab
            details = details + m.format("ddd, MMM DD YYYY") + "%0A";
            m = moment.tz(end_datesMoment + " "+ endTime, home.timeZoneId);
            m.tz(location.timeZoneId);
            details = details + m.format("HH:mm") + "%09"; //%09 = \t = tab
            details = details + m.format("ddd, MMM DD YYYY") + "%0A%0A";
        });

        returnHTML = returnHTML + details + "&trp=true&pli=1&sf=true&output=xml";
        return returnHTML;
    }

    $scope.openInGoogleCalendar = function(){
        //detect home
        var home; 
        var hasHome = false;
        angular.forEach($scope.locations,function(location, index){
            if(location.isHome == true){
                home = location; 
                hasHome = true;                     
            }    
        });
        if(!hasHome){
            alert('Please select a home');
            return;
        }
        var dates = getGoogleDateBaseOnHome(home);
        title = "Let's meet!";
        var googleCalendarURL = renderGoogleCalendarURL(title, dates, home, $scope.locations);
        window.open(googleCalendarURL)
    }

    //this function is used to save event in current session to database, and return it's id to send to /event/:id
    $scope.saveEvent = function(){
        var hasHome = false;
        angular.forEach($scope.locations, function(location, index){
            if(location.isHome == true){
                hasHome = true;
                return;
            }
        });

        if(!hasHome){
            alert('Please select a home');
            return;
        }

        $http({
            method: 'POST',
            url: '/saveEvent',
            data: {
                locations: $scope.locations,
                homeDaysRange: $scope.homeDaysRange,
                homeTimeRange: $scope.homeTimeRange
            }
        }).then(function(res){
            window.location = '/event#/'+res.data._id;
        });
    }

    // Linh - Get time string: YYYY-mm-dd
    $scope.getTimeString = function (time){
        var month = time.getMonth() + 1;
        if (month < 10) {
            month = '0' + month;
        }

        var date = time.getDate();
        if (date < 10) {
            date = '0' + date;
        }

        return time.getFullYear() + '-' + month + '-' + date;     
    }

    // Linh - create tiles array
    $scope.pushTiles = function (tiles, standardZone, timeZoneId, timeStringTemp){
        var timeString;
        var time;
        var timeConvert;
        var timeObject;
            for (var i = 0; i < 24; i++) {
                if (i < 10) {
                    timeString = timeStringTemp + ' 0' + i + ':00:00';
                } else {
                    timeString = timeStringTemp + ' ' + i + ':00:00';
                }
                time = moment.tz(timeString, standardZone);
                timeConvert = time.tz(timeZoneId).format();
                console.log(timeConvert);

                timeObject = {
                    year: timeConvert.slice(0, 4),
                    month: timeConvert.slice(5, 7),
                    date: timeConvert.slice(8, 10),
                    hour: timeConvert.slice(11, 13),
                    minute: timeConvert.slice(14, 16),
                    second: timeConvert.slice(17, 19),
                    zone: timeConvert.slice(19, 22),
                    classCss: $scope.classCss(timeConvert.slice(11, 13))
                };

                tiles.push(timeObject);
            }   
    }

    $scope.classCss = function (hour){
        var classCss;
                switch(hour) {
                    case '00':
                        classCss = 'sleepTime startTime';
                        break;                        
                    case '01':
                    case '02':
                    case '03':
                    case '04':
                    case '05':
                    case '23':
                        classCss = 'sleepTime';
                        break;
                    case '08':
                    case '09':
                    case '10':
                    case '11':
                    case '12':
                    case '13':
                    case '14':
                    case '15':
                    case '16':
                    case '17':
                        classCss = 'businessTime';
                        break;
                    default: classCss = 'freeTime';
                }
        return classCss;              
    }

    $scope.updateTilesView = function (homeIndex){
        var startTimeIndex;
        var tilesViewItem;

        if ($scope.locations.length != 0) {
            for (var i = 0; i < $scope.locations[homeIndex].tiles.length; i++) {
                if ($scope.locations[homeIndex].tiles[i].hour == '00' && $scope.locations[homeIndex].tiles[i].date == $scope.date.getDate()) {
                    startTimeIndex = i;
                    break;              
                }         
            }

            console.log('startTimeIndex = ' + startTimeIndex);

            for (var i = 0; i < $scope.locations.length; i++) {
                $scope.locations[i].tilesView = [];
                var idJ = 0;
                for (var j = startTimeIndex; j < startTimeIndex + 24; j++) {
                    tilesViewItem = JSON.parse(JSON.stringify($scope.locations[i].tiles[j]));
                    switch (tilesViewItem.hour){
                        case '00':
                            tilesViewItem.hour = tilesViewItem.month + ' ' + tilesViewItem.date;
                            break;
                        case '01':
                        case '02':
                        case '03':
                        case '04':
                        case '05':
                        case '06':
                        case '07':
                        case '08':                     
                        case '09':
                            tilesViewItem.hour = tilesViewItem.hour.slice(1, 2);
                    }

                    tilesViewItem.id =  i + '-' + idJ;
                    idJ++;

                    $scope.locations[i].tilesView.push(tilesViewItem);
                }
            }                      
        }        
    }

    $scope.$watch('date', function (){

        $http({
            method: 'POST',
            url: '/saveDateToSession',
            data: {
                date: $scope.date
            }
        });   

        angular.forEach($scope.locations,function(location, index){
            var momentFormatted = moment.tz($scope.date, location.timeZoneId);
            location.timezone_z = momentFormatted.format("z");
            location.timezone_Z = momentFormatted.format("Z");

            var yesterday = new Date($scope.date.getTime());
            yesterday.setDate(yesterday.getDate() - 1);

            var today = new Date($scope.date.getTime());

            var tomorrow = new Date($scope.date.getTime());
            tomorrow.setDate(tomorrow.getDate() + 1);

            // Yesterday
            var timeStringTemp = $scope.getTimeString(yesterday);        
            $scope.pushTiles(tiles, standardZone, location.timeZoneId, timeStringTemp);

            // Today
            timeStringTemp = $scope.getTimeString(today);
            $scope.pushTiles(tiles, standardZone, location.timeZoneId, timeStringTemp);

            // Tomorrow
            timeStringTemp = $scope.getTimeString(tomorrow);
            $scope.pushTiles(tiles, standardZone, location.timeZoneId, timeStringTemp);

            location.tiles = tiles;
            tiles = [];    
        });

        var homeIndex = $scope.findHome();
        $scope.updateTilesView(homeIndex);

        if ($scope.locations.length != 0) {
            $scope.saveLocationsToSession();         
        }
    });

    $scope.timeSelector = {
        display: 'block',
        width: 29,
        height: 0,
        position: 'absolute',
        top: 0,
        left: 0,
        style: ''
    };

    new ResizeSensor($('#sort'), function(){
        $scope.timeSelector.height = $('#sort').height();
        $scope.timeSelector.style = 'display: ' + $scope.timeSelector.display + '; width: ' + $scope.timeSelector.width + 'px; ' 
                                    + 'height: ' + $scope.timeSelector.height + 'px; '
                                    + 'position: ' + $scope.timeSelector.position + '; ' 
                                    + 'top: ' + $scope.timeSelector.top + 'px; ' + 'left: ' + $scope.timeSelector.left + 'px;';       
    });

    // Initialization
    $scope.onFirstBtnClickResult = "";
    $scope.secondBtnInput = "";
    $scope.onDblClickResult = "";
    $scope.onMouseDownResult = "";
    $scope.onMouseUpResult = "";
    $scope.onMouseEnterResult = "";
    $scope.onMouseLeaveResult = "";
    $scope.onMouseMoveResult = "";
    $scope.onMouseOverResult = "";

    // Utility functions

    // Accepts a MouseEvent as input and returns the x and y
    // coordinates relative to the target element.
    var getCrossBrowserElementCoords = function (mouseEvent) {
      var result = {
        x: 0,
        y: 0
      };

      if (!mouseEvent)
      {
        mouseEvent = window.event;
      }

      if (mouseEvent.pageX || mouseEvent.pageY)
      {
        result.x = mouseEvent.pageX;
        result.y = mouseEvent.pageY;
      }
      else if (mouseEvent.clientX || mouseEvent.clientY)
      {
        result.x = mouseEvent.clientX + document.body.scrollLeft +
          document.documentElement.scrollLeft;
        result.y = mouseEvent.clientY + document.body.scrollTop +
          document.documentElement.scrollTop;
      }

      if (mouseEvent.target)
      {
        var offEl = mouseEvent.target;
        var offX = 0;
        var offY = 0;

        if (typeof(offEl.offsetParent) != "undefined")
        {
          while (offEl)
          {
            offX += offEl.offsetLeft;
            offY += offEl.offsetTop;

            offEl = offEl.offsetParent;
          }
        }
        else
        {
          offX = offEl.x;
          offY = offEl.y;
        }

        result.x -= offX;
        result.y -= offY;
      }

      return result;
    };

    var getMouseEventResult = function (mouseEvent, mouseEventDesc)
    {
        var coords = getCrossBrowserElementCoords(mouseEvent);
        // $scope.timeSelector.left = coords.x + 350;
        // $scope.timeSelector.style = 'display: ' + $scope.timeSelector.display + '; width: ' + $scope.timeSelector.width + 'px; ' 
        //                         + 'height: ' + $scope.timeSelector.height + 'px; '
        //                         + 'position: ' + $scope.timeSelector.position + '; ' 
        //                         + 'top: ' + $scope.timeSelector.top + 'px; ' + 'left: ' + $scope.timeSelector.left + 'px';      
        return mouseEventDesc + " at (" + coords.x + ", " + coords.y + ")";
        //return $scope.timeSelector.style;
    };

    // Event handlers
    $scope.onFirstBtnClick = function () {
      $scope.onFirstBtnClickResult = "CLICKED";
    };

    $scope.onSecondBtnClick = function (value) {
      $scope.onSecondBtnClickResult = "you typed '" + value + "'";
    };

    $scope.onDblClick = function () {
      $scope.onDblClickResult = "DOUBLE-CLICKED";
    };

    $scope.onMouseDown = function ($event) {
        isDown = true;
        if(isLeave && isDown){
            $scope.timeSelector.width = 29;
            $scope.timeSelector.style = 'display: ' + $scope.timeSelector.display + '; width: ' + $scope.timeSelector.width + 'px; ' 
                                    + 'height: ' + $scope.timeSelector.height + 'px; '
                                    + 'position: ' + $scope.timeSelector.position + '; ' 
                                    + 'top: ' + $scope.timeSelector.top + 'px; ' + 'left: ' + $scope.timeSelector.left + 'px';  
        }
        isLeave = false;
        isDown = false;

        angular.forEach($scope.locations, function(location, index){
            location.timeRangeView = ''; 
        });
        
        $scope.clockIcon = "position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: scroll; z-index: -1; visibility: hidden;";

        $scope.tilesStyle = 'padding-top: 20px; margin-right: 25px;';         

        $scope.onMouseDownResult = getMouseEventResult($event, "Mouse down");
    };

    $scope.onMouseUp = function ($event) {
      $scope.onMouseUpResult = getMouseEventResult($event, "Mouse up");
    };

    $scope.onMouseEnter = function ($event) {
      $scope.onMouseEnterResult = getMouseEventResult($event, "Mouse enter");
    };

    $scope.onMouseLeave = function ($event) {
      $scope.onMouseLeaveResult = getMouseEventResult($event, "Mouse leave");
    };

    $scope.onMouseMove = function ($event) {
      $scope.onMouseMoveResult = getMouseEventResult($event, "Mouse move");
    };

    $scope.onMouseOver = function ($event) {
      $scope.onMouseOverResult = getMouseEventResult($event, "Mouse over");
    };

    // My mouse
    $scope.myRange = '';
    var startRangeId = '';
    var endRangeId = '';
    var mouseDownPosition;
    var isLeave = false;
    var isDown = false;
    $scope.tilesStyle = 'padding-top: 20px; margin-right: 25px;';
    $scope.clockIcon = "position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: scroll; z-index: -1; visibility: hidden;";
    
    $scope.onSlideBarMouseLeave = function ($event) {
        isLeave = true;       
    };    

    $scope.onMyMouseDown = function ($event) {
        startRangeId = $event.path[0].id;
        startRangeId = startRangeId.split("-")[1];
        console.log($event);
        mouseDownPosition = $event.clientX
        $scope.timeSelector.left = mouseDownPosition - 150;
        $scope.timeSelector.style = 'display: ' + $scope.timeSelector.display + '; width: ' + $scope.timeSelector.width + 'px; ' 
                                + 'height: ' + $scope.timeSelector.height + 'px; '
                                + 'position: ' + $scope.timeSelector.position + '; ' 
                                + 'top: ' + $scope.timeSelector.top + 'px; ' + 'left: ' + $scope.timeSelector.left + 'px';         
    };

    $scope.onMyMouseUp = function ($event) {
        $scope.clockIcon = "";
        $scope.tilesStyle = 'margin-right: 25px;';
        endRangeId = $event.path[0].id;
        endRangeId = endRangeId.split("-")[1];

        $scope.timeSelector.width = $event.clientX - mouseDownPosition + 30;
        $scope.timeSelector.style = 'display: ' + $scope.timeSelector.display + '; width: ' + $scope.timeSelector.width + 'px; ' 
                                + 'height: ' + $scope.timeSelector.height + 'px; '
                                + 'position: ' + $scope.timeSelector.position + '; ' 
                                + 'top: ' + $scope.timeSelector.top + 'px; ' + 'left: ' + $scope.timeSelector.left + 'px';

        function startTimeProcess (hour){
            if(hour.length < 3){
                return hour;
            }else{
                return '00';
            }
        }

        if (startRangeId.length != 0 && endRangeId.length != 0) {
            angular.forEach($scope.locations, function(location, index){
                location.timeRange = {
                    startRange: location.tilesView[startRangeId],
                    endRange: location.tilesView[endRangeId]
                }; 
                location.timeRangeView = startTimeProcess(addZero(location.timeRange.startRange.hour)) + ':'
                                        + location.timeRange.startRange.minute + '-' 
                                        + startTimeProcess(addZero(location.timeRange.endRange.hour)) + ':'
                                        + location.timeRange.endRange.minute;        
            });              
        }

        //please follow this format, if not, the code wont run

        var homeTimeRange = $scope.findHome();
        var tilesStartRange = $scope.locations[homeTimeRange].timeRange.startRange;
        var tilesEndRange = $scope.locations[homeTimeRange].timeRange.endRange;

        //$scope.homeTimeRange = startTimeProcess(addZero(tilesStartRange.hour)) + ':' + tilesStartRange.minute + '-' + startTimeProcess(addZero(tilesEndRange.hour)) + ':' + tilesEndRange.minute;
        
        $scope.homeTimeRange = $scope.locations[homeTimeRange].timeRangeView;

        var myStartDate = new Date(tilesStartRange.year, tilesStartRange.month - 1, tilesStartRange.date);
        var myStartDateArray = myStartDate.toString().split(" ");
        var myStartDateString = myStartDateArray[0] + ', ' + myStartDateArray[1] + ' ' 
                                + myStartDateArray[2] + ' ' +  myStartDateArray[3];

        var myEndDate = new Date(tilesEndRange.year, tilesEndRange.month - 1, tilesEndRange.date);
        var myEndDateArray = myEndDate.toString().split(" ");
        var myEndDateString = myEndDateArray[0] + ', ' + myEndDateArray[1] + ' ' 
                                + myEndDateArray[2] + ' ' +  myEndDateArray[3];

        $scope.homeDaysRange = myStartDateString + '-' + myEndDateString;
    };             
    //region login
    $scope.username;
    $scope.password;
    $scope.content;
    $scope.login = function () {
                //alert($scope.username + " " + $scope.password);
       $http.post('/login', { user: { username: $scope.username, password: $scope.password }})
                .then(function(response) {
           $scope.user = response.data.user;
           $scope.user = response.data.user;     
            $scope.locations = response.data.user.locations;
            angular.forEach($scope.locations,function(location, index){
                location.currentTime = moment.tz(new Date(), location.timeZoneId).format("HH:mm");            
            }); 
                }, function(response) {
                    $scope.content = "Something went wrong";
                });
    }; 
});