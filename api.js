var bodyparser = require('body-parser');
var express = require('express');
var session = require('express-session');
var status = require('http-status');
var _ = require('underscore');
module.exports = function(wagner) {
  var api = express.Router();
  api.use(bodyparser.json());

  api.get('/category/id/:id', wagner.invoke(function(Category) {
    return function(req, res) {
      Category.findOne({ _id: req.params.id }, function(error, category) {
        if (error) {
          return res.
            status(status.INTERNAL_SERVER_ERROR).
            json({ error: error.toString() });
        }
        if (!category) {
          return res.
            status(status.NOT_FOUND).
            json({ error: 'Not found' });
        }
        res.json({ category: category });
      });
    };
  }));

  api.get('/category/parent/:id', wagner.invoke(function(Category) {
    return function(req, res) {
      Category.
        find({ parent: req.params.id }).
        sort({ _id: 1 }).
        exec(function(error, categories) {
          if (error) {
            return res.
              status(status.INTERNAL_SERVER_ERROR).
              json({ error: error.toString() });
          }
          res.json({ categories: categories });
        });
    };
  }));

  api.post('/register', wagner.invoke(function(User) {
    return function(req, res) {
        if(req.body.password != req.body.confirmPassword){
            return res.
                status(status.INTERNAL_SERVER_ERROR).
                json({ error: "Your confirm password doesnt match the origin!" });
        }
        var user = new User({
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            username: req.body.username,
            password: req.body.password
        });
        console.log(user);
        user.save(function(error, user) {
            if (error) {
              return res.
                status(status.INTERNAL_SERVER_ERROR).
                json({ error: error.toString(), user: user });
            }
            res.json({status: 'success'});
        });
      //res.json({ data:req.body });
    };
  }));
  
  return api;
};
