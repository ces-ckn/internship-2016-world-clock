var mongoose = require('mongoose');

module.exports = new mongoose.Schema({ 
    firstName: {
      type: String,
      required: [true, '\n The first name field is required'],
    },
    lastName: {
      type: String,
      required: [true, '\n The last name field is required']
    },
    username: {
      type: String,
      required: [true, '\n The username field is required'],
      minlength: 6
    },
    password: {
      type: String,
      required: [true, '\n The password field is required'],
      minlength: 6
    },
    locations: {
        type: Array,
        "default": []
    }
});

module.exports.set('toObject', { virtuals: true });
module.exports.set('toJSON', { virtuals: true });
