var mongoose = require('mongoose');
var _ = require('underscore');
module.exports = function(wagner) {
	mongoose.connect('mongodb://heroku_lhsjcb7d:uss8clnbn3p5i62oub8403sqj6@ds023455.mlab.com:23455/heroku_lhsjcb7d');	
  	//mongoose.connect('mongodb://localhost:27017/test');

  var Category =
    mongoose.model('Category', require('./category'), 'categories');

  var User = mongoose.model('User', require('./user'), 'users');
  var Event = mongoose.model('Event', require('./event'), 'events');

    var models = {
        User: User,
        Category: Category,
        Event: Event
    }

    _.each(models, function(value, key){
        wagner.factory(key, function(){
            return value;
        });
    });
};
