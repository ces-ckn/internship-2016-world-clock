var mongoose = require('mongoose');

module.exports = new mongoose.Schema({ 
    title: {
      type: String,
      "default": "Let's meet!"
    },
    locations: {
        type: Array,
        "default": []
    },
    description:{
      type: String,
      "default": "Please be on time"
    },
    homeTimeRange: {
      type: String
    },
    homeDaysRange: {
      type: String
    }
});

module.exports.set('toObject', { virtuals: true });
module.exports.set('toJSON', { virtuals: true });
